FROM python:3.6-slim

WORKDIR /opt/lets
ADD . /opt/lets

ENV mysql_usr = "rippleuser" \
        peppy_cikey = "changeme" \
        mysql_psw = "secrez"    \
        lets_osuapikey = "YOUR_OSU_API_KEY_HERE"

RUN apt-get update && apt-get install -y git python-dev mysql-server default-libmysqlclient-dev gcc
RUN git submodule update --init --recursive

# Install any needed packages specified in requirements.txt
RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

RUN python3.6 setup.py build_ext --inplace
RUN python3.6 lets.py
RUN sed -i "s#root#'${mysql_usr}'#g; s#changeme#'${peppy_cikey}'#g; s#YOUR_OSU_API_KEY_HERE#'${lets_osuapikey}'#g" config.ini
RUN sed -E -i -e 'H;1h;$!d;x' config.ini -e "s#password = #password = '${mysql_psw}'#"

EXPOSE 5002

# Run app.py when the container launches
CMD ["python3.6", "lets.py"]
